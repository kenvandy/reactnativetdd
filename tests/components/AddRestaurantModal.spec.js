import React from 'react';
import {shallow} from 'enzyme';
import AddRestaurantModal from '../../src/AddRestaurantModal';

describe('Test Restaurant', () => {
  function testID(id) {
    return (cmp) => cmp.props().testID === id;
  }

  describe('upon submit', () => {
    const messageText = 'Hello World';
    let handleSave;
    let wrapper;

    beforeEach(() => {
      handleSave = jest.fn();
      wrapper = shallow(
        <AddRestaurantModal onSave={handleSave} isVisibleModal={true} />,
      );
      wrapper
        .findWhere(testID('restaurantNameTextField'))
        .simulate('changeText', messageText);

      wrapper.findWhere(testID('saveRestaurantButton')).simulate('press');
    });

    it('Clear the text filed when submitted', () => {
      expect(
        wrapper.findWhere(testID('restaurantNameTextField')).props()
          .defaultValue,
      ).toEqual('');
    });

    it('Call the onSave handler with the entered text', () => {
      expect(handleSave).toHaveBeenCalledWith(messageText);
    });
  });

  describe('upon submit with invalid data', () => {
    let handleSave;
    let wrapper;

    beforeEach(() => {
      handleSave = jest.fn();
      wrapper = shallow(
        <AddRestaurantModal onSave={handleSave} isVisibleModal={true} />,
      );

      wrapper.findWhere(testID('saveRestaurantButton')).simulate('press');
    });

    it('Display the error message', () => {
      expect(
        wrapper.findWhere(testID('restaurantNameTextField')).props()
          .errorMessage,
      ).toEqual('Required');
    });

    it('does not call the onSave handler', () => {
      expect(handleSave).not.toHaveBeenCalled();
    });
  });

  describe('upon cancellation', () => {
    let wrapper;
    let handleCancel;

    beforeEach(() => {
      handleCancel = jest.fn();

      wrapper = shallow(
        <AddRestaurantModal isVisibleModal={true} onCancel={handleCancel} />,
      );
      wrapper
        .findWhere(testID('restaurantNameTextField'))
        .simulate('changeText', 'Hello World');

      wrapper.findWhere(testID('cancelAddRestaurantButton')).simulate('press');
    });

    it('Clear the text filed when submitted', () => {
      expect(
        wrapper.findWhere(testID('restaurantNameTextField')).props()
          .defaultValue,
      ).toEqual('');
    });
    it('Call the onCancel handler', () => {
      expect(handleCancel).toHaveBeenCalled();
    });
  });
});
