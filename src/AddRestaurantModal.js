import React, {useState} from 'react';
import {View, StyleSheet, Modal} from 'react-native';
import {Button, Input} from 'react-native-elements';

function AddRestaurantModal({isVisibleModal, onSave, onCancel}) {
  const [restaurantName, setRestaurantName] = useState('');

  const handlePressSaveButton = () => {
    if (restaurantName.length) {
      setRestaurantName('');
      onSave(restaurantName);
    }
    return;
  };
  const handleCancelSaveButton = () => {
    setRestaurantName('');
    onCancel();
  };

  const handleChangeText = (text) => {
    setRestaurantName(text);
  };

  if (!isVisibleModal) {
    return null;
  }
  return (
    <Modal
      style={styles.container}
      visible={isVisibleModal}
      animationType={'slide'}>
      <View style={styles.wrapper}>
        <Input
          testID="restaurantNameTextField"
          style={styles.textInput}
          defaultValue={restaurantName}
          onChangeText={handleChangeText}
          label="Retaurant Name"
          autoFocus
          errorMessage={!restaurantName.length ? 'Required' : false}
        />
        <Button
          testID="saveRestaurantButton"
          title="Save Restaurant"
          onPress={handlePressSaveButton}
        />
        <Button
          testID="cancelAddRestaurantButton"
          title="Cancel"
          style={styles.cancelBtn}
          onPress={handleCancelSaveButton}
        />
      </View>
    </Modal>
  );
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  },
  textInput: {
    fontSize: 18,
  },
  wrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  cancelBtn: {
    marginTop: 15,
  },
});

export default AddRestaurantModal;
