import React, {useState} from 'react';
import {View, StyleSheet, FlatList} from 'react-native';
import {Button, ListItem, Text} from 'react-native-elements';

import AddRestaurantModal from './AddRestaurantModal';

function RestaurantList() {
  const [isVisibleModal, setVisibleModal] = useState(false);
  const [restaurantNames, setRestaurantNames] = useState([]);

  const handleAddRestaurant = (newRestaurantName) => {
    setVisibleModal(false);
    setRestaurantNames((prevState) => [...prevState, newRestaurantName]);
  };

  const renderItem = ({item}) => {
    return (
      <ListItem bottomDivider>
        <ListItem.Content>
          <ListItem.Title>{item}</ListItem.Title>
        </ListItem.Content>
        <ListItem.Chevron />
      </ListItem>
    );
  };

  const renderList = () => {
    return (
      <View style={styles.flatList}>
        <FlatList
          data={restaurantNames}
          renderItem={renderItem}
          keyExtractor={(item, index) => item}
          extraData={restaurantNames}
        />
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <Button
        testID="newRestaurantButton"
        title="New Restaurant"
        onPress={() => setVisibleModal(true)}
      />
      <AddRestaurantModal
        setVisibleModal={setVisibleModal}
        isVisibleModal={isVisibleModal}
        onSave={handleAddRestaurant}
        onCancel={() => setVisibleModal(false)}
      />
      <View style={styles.dummy} />
      {renderList()}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    marginTop: 20,
  },
  h2Style: {
    marginBottom: 20,
  },
  flatList: {
    width: '100%',
    marginTop: 20,
  },
  item: {
    fontSize: 20,
  },
});

export default RestaurantList;
