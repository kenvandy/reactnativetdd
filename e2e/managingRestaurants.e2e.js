describe('Managing Restaurant', () => {
  beforeAll(async () => {
    await device.launchApp();
  });

  it('Should allow creating restaurant', async () => {
    const restaurantName = 'Sushi Place';
    // Click add Restaurant button
    await element(by.id('newRestaurantButton')).tap();
    // Auto Focus
    await expect(element(by.id('restaurantNameTextField'))).toBeFocused();
    // Enter restaurant name
    await element(by.id('restaurantNameTextField')).typeText(restaurantName);
    // Click Save button
    await element(by.id('saveRestaurantButton')).tap();
    // Check to make sure Restaurant appears in the list
    await expect(element(by.label(restaurantName))).toBeVisible();
    await expect(element(by.id('restaurantNameTextField'))).toBeNotVisible();
  });
});
